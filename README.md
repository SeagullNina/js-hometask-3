# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: 1) Использовать ссылку на this во внутренней переменной; 2) Использовать Bind, первым параметром передавая ссылку на новый контекст; 3) Присвоить новый контекст через Call; 4) Присвоить новый контекст через Apply. 5) Использование стрелочной функции позаимствует контекст из окружения функции. 
Разница Call и Apply в том, что в первом случае аргументы передаются через запятую, а во втором - через массив. Разница между Bind и Call с Apply в том, что в первом случае функция не выполняется автоматически.
#### 2. Что такое стрелочная функция?
> Ответ: Укороченный способ записи функции, который решает проблему контекста - стрелочная функция не имеет собственного this и захватывает значение из окружающего её контекста.
#### 3. Приведите свой пример конструктора. 
```js
// Ответ:
function Bird(name, weight, height) {
  this.name = name;
  this.my = true;
  this.weight = weight;
  this.height = height;
}

const myBird = new Bird('Seagull', 3, 50);
```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
//c помощью this
  const person = {
    name: 'Nikita',
    sayHello: function() {
	  const that = this;
      setTimeout(function() {
          console.log(that.name + ' says hello to everyone!');
      }, 1000)
    }
  }
  person.sayHello();
  
//с помощью bind
	 const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(this), 1000)
    }
  }
    person.sayHello();
	
//стрелочная функция
	 const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(() => console.log(this.name + ' says hello to everyone!'), 1000);
    }
  }
    person.sayHello();
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
